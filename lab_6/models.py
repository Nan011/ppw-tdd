from django.db import models
from django.utils import timezone

# Create your models here.
NAME_LENGTH = 30;
DESC_LENGTH = 300;
class StatusPost(models.Model):
	description = models.TextField(max_length=DESC_LENGTH)
	date = models.DateTimeField("Published date", default=timezone.now, blank=True)

class Accordion(models.Model):
	name = models.CharField(max_length=DESC_LENGTH)
	title1 = models.CharField(max_length=DESC_LENGTH)
	description1 = models.TextField(max_length=DESC_LENGTH)
	title2 = models.CharField(max_length=DESC_LENGTH)
	description2 = models.TextField(max_length=DESC_LENGTH)
	title3 = models.CharField(max_length=DESC_LENGTH)
	description3 = models.TextField(max_length=DESC_LENGTH)
	title4 = models.CharField(max_length=DESC_LENGTH)
	description4 = models.TextField(max_length=DESC_LENGTH)

class Subscriber(models.Model):
	name = models.CharField(max_length=NAME_LENGTH)
	email = models.CharField(max_length=NAME_LENGTH)
	password = models.CharField(max_length=NAME_LENGTH)