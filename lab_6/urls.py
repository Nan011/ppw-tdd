from django.urls import path
from . import views
urlpatterns = [
	path("", views.index, name="index"),
	path("profile/", views.profile, name="profile"),
	path("profile/subscribe/", views.subscribe, name="subscribe"),
	path("profile/subscribe/validate/", views.validate_subscriber),
	path("profile/subscribe/add/", views.add_subscriber),
	path("profile/subscribe/del/", views.del_subscriber),
	path("profile/subscribe/get/subscriber/", views.get_subscriber),
	path("delete_status/<int:pk>/", views.delete_status, name="delete status"),
]