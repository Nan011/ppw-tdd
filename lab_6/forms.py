from django import forms

DESC_LENTH = 300
class StatusPostForm(forms.Form):
	description = forms.CharField(max_length=DESC_LENTH, widget=forms.Textarea(
		attrs={
			"class": "status-post",
			"placeholder": "Tell me, what are you thinking now?",
			# "onclick": "larging(1)"
		}
	))

class SubscribeForm(forms.Form):
	email = forms.CharField(widget=forms.EmailInput(
		attrs={
			"class": "c-subscribe__email",
			"placeholder": "Email",
		}
	))

	name = forms.CharField(widget=forms.TextInput(
		attrs={
			"class": "c-subscribe__name",
			"placeholder": "Name",
		}
	))

	password = forms.CharField(widget=forms.PasswordInput(
		attrs={
			"class": "c-subscribe__password",
			"placeholder": "Password",
		}
	))