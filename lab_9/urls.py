from django.urls import path;
from lab_9 import views;
urlpatterns = [
    path("books/", views.index),
    path("api/books/", views.books_api),
    path("login/", views.login),
    path("user/update/<str:token>/", views.update_user),
    path("user/check/session/", views.check_session),
    path("user/clear/session/", views.clear_session),
    path("api/books/<str:name>/", views.select_books_api),
    path("user/<str:token>/", views.user),
]