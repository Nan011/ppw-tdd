function getBooks(url, name) {
    countStar = 0;
    $("#books").remove();
    $("table").append("<tbody id='books'></tbody>");
    console.log("masuk");
    $.ajax({    
        url: url + name + "/",
        dataType: "json",
        success: function(data) {
            let dataString = "";
            $.each(data.books, function(key, value) {
                dataString += "<tr>";
                dataString += "<td>" + value.volumeInfo.title + "</td>";
                dataString += "<td>" + value.volumeInfo.authors + "</td>";
                dataString += "<td>" + value.volumeInfo.publisher + "</td>";
                dataString += "<td>" + value.volumeInfo.averageRating + "</td>";
                if ($.cookie("userBooks").indexOf(value.id) >= 0) {
                    dataString += `<td><button class='favor-button marked-favor-button' data-id=${value.id}></button></td>`;
                    countStar++;
                } else {
                    dataString += `<td><button class='favor-button' data-id=${value.id}></button></td>`;
                }
                dataString += "</tr>";
            });
            $(".counter-number").text(countStar);
            $("#books").append(dataString);
        }
    });
}

function setUserSessionFromServer(tokenId) {
    $.ajax({
        headers: {"X-CSRFToken": $.cookie("csrftoken")},
        method: "POST",
        url: `/lab-9/user/${tokenId}`,
        dataType: "json",
        success: function(data) {
            if (data.user_exists) {
                console.log("server data:" + data.fav_books)
                $.cookie("userBooks", data.fav_books);
            }
        },
    });
}

function updateUser(add, id) {
    $.ajax({
        headers: {"X-CSRFToken": $.cookie("csrftoken")},
        method: "POST",
        url: `/lab-9/user/update/${$.cookie("id_token")}/`,
        data: {
            "opeType": add,
            "bookId": id,
        },
        dataType: "json",
        success: function(data) {
            if (data.success) {
                console.log(data.success);
                $.cookie("userBooks", data.fav_books);
                console.log("update: " + data.fav_books);
            }
        },
    });
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    $(".l-google-user").remove();
    let newTag = "<div class='l-google-user'></div>";
    $(".l-google-user-auth").append(newTag);
    $(".c-notif__message").removeClass("j-notif__animation");
    $(".c-notif__message").text("");    
    $(".c-wall").removeClass("j-wall__animation");
    $("#to-login").removeClass("hide");
    $.cookie("userBooks", "");
    getBooks("/lab-9/api/books/", $.cookie("currKeyword"));
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
    $.ajax({
        url: `/lab-9/user/clear/session/`,
        dataType: "json",
        success: function(data) {
            if (data.success) {
                console.log("nice");
            }
        },
    });
}

function getBooksWithSession() {
    $.ajax({
        url: `/lab-9/user/check/session/`,
        dataType: "json",
        success: function(data) {
            if (data.user_exists) {
                $.cookie("userBooks", data.user_books);
                $("#to-login").addClass("hide");
                let user_tag = `<img src="${data.image_url}" class="c-user__photo">`;
                user_tag += `<div class="l-user--right"><label>${data.name}</label>`;
                user_tag += `<a class='l-google__sign-out' href='#' onclick='signOut();'><div class='c-google__sign-out'>Sign Out</div></a></div>`;
                $(".l-google-user").append(user_tag);
                $(".c-notif__message").addClass("j-notif__animation");
                $(".c-wall").addClass("j-wall__animation");
                $(".c-notif__message").text(`Selamat datang, ${data.name}`);
            } else {
                $.cookie("userBooks", "");
            }
            getBooks("/lab-9/api/books/", $.cookie("currKeyword"));
        },
    });
}

$("document").ready(function() {
    if ($.cookie("currKeyword") == undefined) {
        $.cookie("currKeyword", "quilting");
    }
    getBooksWithSession();

    $("#to-login").on("click", function() {
        window.location.replace("/lab-9/login/");
    });

    $("table").on("click", ".favor-button", function() {
        if ($(this).attr("class").split(" ").length == 1) {
            $(this).addClass("marked-favor-button");
            updateUser(true, $(this).attr("data-id"));
            $(".counter-number").text(eval($(".counter-number").text()) + 1);
        } else {
            $(this).removeClass("marked-favor-button");
            updateUser(false, $(this).attr("data-id"));
            $(".counter-number").text(eval($(".counter-number").text()) - 1);
        }
    });

    $("#search-button").on("click", function() {
        let name = $(".search-input").val();
        if (name === "") {
            $.cookie("currKeyword", "quilting");
        } else {
            $.cookie("currKeyword", name);
        }
        getBooks("/lab-9/api/books/", $.cookie("currKeyword"));
    });
});

